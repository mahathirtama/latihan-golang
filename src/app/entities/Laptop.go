package entities

import "gorm.io/gorm"

type Laptop struct {
	gorm.Model
	SerialNumber string `json:"serial_number" gorm:"column:serial_number`
	Merk         string `json:"merk" gorm:"column:merk`
}