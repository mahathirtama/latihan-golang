package entities

import "gorm.io/gorm"

type Pegawai struct {
	gorm.Model
	Name string `json:"name" gorm:"column:name`
}
